package pl.nejos.unsplashviewer.common.extensions

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import pl.nejos.unsplashviewer.domain.exception.UnsplashViewerException

fun <T> Flow<T>.catchUnsplashViewerException(action: (exception: UnsplashViewerException) -> Unit): Flow<T> =
    catch {
        if (it is UnsplashViewerException) {
            action(it)
        }
    }
