package pl.nejos.unsplashviewer.common.extensions

import pl.nejos.unsplashviewer.domain.exception.UnsplashViewerException
import java.io.IOException
import java.net.UnknownHostException

fun Throwable.asUnsplashViewerException(): UnsplashViewerException =
    when (this) {
        is UnknownHostException -> UnsplashViewerException.ConnectionException
        is IOException -> UnsplashViewerException.ConnectionException
        else -> UnsplashViewerException.UnknownException
    }
