package pl.nejos.unsplashviewer.common.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class UnsplashViewerApplication : Application()
