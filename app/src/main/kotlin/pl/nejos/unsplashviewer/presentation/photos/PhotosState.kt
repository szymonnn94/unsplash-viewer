package pl.nejos.unsplashviewer.presentation.photos

import androidx.annotation.StringRes
import pl.nejos.unsplashviewer.domain.entity.Photo

sealed class PhotosState {
    object Loading: PhotosState()
    data class Success(val photos: List<Photo>): PhotosState()
    data class Failure(@StringRes val displayMessage: Int): PhotosState()
}
