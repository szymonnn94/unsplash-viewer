package pl.nejos.unsplashviewer.presentation.photos

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import pl.nejos.unsplashviewer.common.extensions.catchUnsplashViewerException
import pl.nejos.unsplashviewer.domain.usecase.GetPhotosUseCase
import javax.inject.Inject

@HiltViewModel
class PhotosViewModel @Inject constructor(
    private val getPhotosUseCase: GetPhotosUseCase
) : ViewModel() {
    private val state = MutableLiveData<PhotosState>()
    val stateLiveData: LiveData<PhotosState> = state

    fun init() {
        viewModelScope.launch {
            getPhotosUseCase.call()
                .onStart { state.value = PhotosState.Loading }
                .onEach { state.value = PhotosState.Success(it) }
                .catchUnsplashViewerException {
                    state.value = PhotosState.Failure(it.displayMessage)
                }
                .collect()
        }
    }
}
