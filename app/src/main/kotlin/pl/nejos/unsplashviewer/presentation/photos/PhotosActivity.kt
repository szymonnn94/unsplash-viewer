package pl.nejos.unsplashviewer.presentation.photos

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import pl.nejos.unsplashviewer.R
import pl.nejos.unsplashviewer.databinding.ActivityPhotosBinding
import pl.nejos.unsplashviewer.domain.entity.Photo

@AndroidEntryPoint
class PhotosActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPhotosBinding
    private val viewModel: PhotosViewModel by viewModels()
    private val photosAdapter = PhotosAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViews()
        setupStateObserver()
        viewModel.init()
    }

    private fun setupViews() {
        binding = ActivityPhotosBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.photosRecyclerView.adapter = photosAdapter
        binding.photosRecyclerView.layoutManager = GridLayoutManager(this, ITEMS_IN_ROW)
    }

    private fun setupStateObserver() {
        viewModel.stateLiveData.observe(this) { state ->
            when (state) {
                is PhotosState.Loading -> showLoading()
                is PhotosState.Failure -> showFailure(state.displayMessage)
                is PhotosState.Success -> showSuccess(state.photos)
            }
        }
    }

    private fun showLoading() {
        binding.progressBar.isVisible = true
        binding.errorTextView.isVisible = false
        binding.photosRecyclerView.isVisible = false
    }

    private fun showFailure(@StringRes displayMessage: Int) {
        binding.progressBar.isVisible = false
        binding.photosRecyclerView.isVisible = false
        binding.errorTextView.isVisible = true
        binding.errorTextView.setText(displayMessage)
    }

    private fun showSuccess(photos: List<Photo>) {
        binding.progressBar.isVisible = false
        binding.errorTextView.isVisible = false
        binding.photosRecyclerView.isVisible = true
        photosAdapter.submitList(photos)
    }

    companion object {
        private const val ITEMS_IN_ROW = 4
    }
}
