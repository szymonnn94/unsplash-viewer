package pl.nejos.unsplashviewer.data.photos.datasource

import kotlinx.coroutines.flow.Flow
import pl.nejos.unsplashviewer.data.photos.entity.PhotoEntity

interface PhotosDataSource {
    fun fetchPhotoEntities(): Flow<List<PhotoEntity>>
}
