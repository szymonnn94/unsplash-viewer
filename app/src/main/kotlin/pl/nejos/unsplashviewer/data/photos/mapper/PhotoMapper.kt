package pl.nejos.unsplashviewer.data.photos.mapper

import pl.nejos.unsplashviewer.data.common.Mapper
import pl.nejos.unsplashviewer.data.photos.entity.PhotoEntity
import pl.nejos.unsplashviewer.domain.entity.Photo
import javax.inject.Inject

class PhotoMapper @Inject constructor() : Mapper<PhotoEntity, Photo> {
    override fun from(from: PhotoEntity): Photo =
        Photo(id = from.id, url = from.urls.regular)

    override fun fromList(from: List<PhotoEntity>): List<Photo> =
        from.map(::from)
}
