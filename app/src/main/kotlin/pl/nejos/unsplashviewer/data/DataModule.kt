package pl.nejos.unsplashviewer.data

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import pl.nejos.unsplashviewer.BuildConfig
import pl.nejos.unsplashviewer.data.photos.PhotosDataModule
import retrofit2.Converter
import retrofit2.Retrofit

@Module(includes = [PhotosDataModule::class])
@InstallIn(SingletonComponent::class)
class DataModule {
    @Provides
    fun provideRetrofit(jsonConverterFactory: Converter.Factory): Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(jsonConverterFactory)
        .build()

    @ExperimentalSerializationApi
    @Provides
    fun provideJsonConverterFactory(): Converter.Factory =
        Json { ignoreUnknownKeys = true }.asConverterFactory(MediaType.get(MEDIA_TYPE))

    companion object {
        private const val MEDIA_TYPE = "application/json"
    }
}
