package pl.nejos.unsplashviewer.data.photos

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flatMap
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.map
import pl.nejos.unsplashviewer.data.photos.datasource.PhotosNetworkDataSource
import pl.nejos.unsplashviewer.data.photos.mapper.PhotoMapper
import pl.nejos.unsplashviewer.domain.entity.Photo
import pl.nejos.unsplashviewer.domain.repository.PhotosRepository
import javax.inject.Inject

class PhotosRepositoryImpl @Inject constructor(
    private val photosNetworkDataSource: PhotosNetworkDataSource,
    private val photoMapper: PhotoMapper
) : PhotosRepository {
    override fun fetchPhotos(): Flow<List<Photo>> =
        photosNetworkDataSource.fetchPhotoEntities()
            .map(photoMapper::fromList)
}
