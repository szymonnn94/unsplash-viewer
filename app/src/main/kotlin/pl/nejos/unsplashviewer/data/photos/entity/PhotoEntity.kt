package pl.nejos.unsplashviewer.data.photos.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PhotoEntity (
    @SerialName("id") val id: String,
    @SerialName("urls") val urls: UrlsEntity
)
