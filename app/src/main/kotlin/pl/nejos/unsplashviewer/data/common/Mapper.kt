package pl.nejos.unsplashviewer.data.common

interface Mapper<From, To> {
    fun from(from: From): To

    fun fromList(from: List<From>): List<To>
}
