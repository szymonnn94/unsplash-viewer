package pl.nejos.unsplashviewer.data.photos.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UrlsEntity(
    @SerialName("regular") val regular: String
)
