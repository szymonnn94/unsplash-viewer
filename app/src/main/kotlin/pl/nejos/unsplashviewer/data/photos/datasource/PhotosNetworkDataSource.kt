package pl.nejos.unsplashviewer.data.photos.datasource

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import pl.nejos.unsplashviewer.BuildConfig
import pl.nejos.unsplashviewer.data.common.ErrorMapper
import pl.nejos.unsplashviewer.data.photos.api.PhotosService
import pl.nejos.unsplashviewer.data.photos.entity.PhotoEntity
import javax.inject.Inject

class PhotosNetworkDataSource @Inject constructor(
    private val photosService: PhotosService,
    private val errorMapper: ErrorMapper
) : PhotosDataSource {

    override fun fetchPhotoEntities(): Flow<List<PhotoEntity>> =
        flow {
            emit(photosService.getPhotos(BuildConfig.CLIENT_ID))
        }.catch {
            throw errorMapper.map(it)
        }
}
