package pl.nejos.unsplashviewer.data.photos

import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import pl.nejos.unsplashviewer.data.photos.api.PhotosService
import pl.nejos.unsplashviewer.domain.repository.PhotosRepository
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
interface PhotosDataModule {
    @Binds
    fun bindPhotosRepository(photosRepositoryImpl: PhotosRepositoryImpl): PhotosRepository

    companion object {
        @Provides
        fun providePhotosService(retrofit: Retrofit): PhotosService =
            retrofit.create(PhotosService::class.java)
    }
}
