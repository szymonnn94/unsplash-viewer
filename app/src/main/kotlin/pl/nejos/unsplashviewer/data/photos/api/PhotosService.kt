package pl.nejos.unsplashviewer.data.photos.api

import pl.nejos.unsplashviewer.data.photos.entity.PhotoEntity
import retrofit2.http.GET
import retrofit2.http.Query

interface PhotosService {

    @GET("photos")
    suspend fun getPhotos(@Query("client_id") clientId: String): List<PhotoEntity>
}
