package pl.nejos.unsplashviewer.data.common

import pl.nejos.unsplashviewer.common.extensions.asUnsplashViewerException
import pl.nejos.unsplashviewer.domain.exception.UnsplashViewerException
import javax.inject.Inject

class ErrorMapper @Inject constructor() {
    fun map(e: Throwable): UnsplashViewerException {
        return e.asUnsplashViewerException()
    }
}
