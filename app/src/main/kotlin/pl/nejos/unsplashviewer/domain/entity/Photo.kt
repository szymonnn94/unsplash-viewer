package pl.nejos.unsplashviewer.domain.entity

data class Photo(
    val id: String,
    val url: String
)
