package pl.nejos.unsplashviewer.domain.usecase

import kotlinx.coroutines.flow.Flow

interface FlowUseCase<Response> {
    fun call(): Flow<Response>
}
