package pl.nejos.unsplashviewer.domain.exception

import androidx.annotation.StringRes
import pl.nejos.unsplashviewer.R

sealed class UnsplashViewerException(
    @StringRes val displayMessage: Int
) : Exception() {
    object ConnectionException : UnsplashViewerException(R.string.error_connection)
    object UnknownException : UnsplashViewerException(R.string.error_unknown)
}
