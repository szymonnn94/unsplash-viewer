package pl.nejos.unsplashviewer.domain.repository

import kotlinx.coroutines.flow.Flow
import pl.nejos.unsplashviewer.domain.entity.Photo

interface PhotosRepository {
    fun fetchPhotos(): Flow<List<Photo>>
}
