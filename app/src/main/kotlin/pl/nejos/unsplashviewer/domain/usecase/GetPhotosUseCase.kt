package pl.nejos.unsplashviewer.domain.usecase

import kotlinx.coroutines.flow.Flow
import pl.nejos.unsplashviewer.domain.entity.Photo
import pl.nejos.unsplashviewer.domain.repository.PhotosRepository
import javax.inject.Inject

class GetPhotosUseCase @Inject constructor(
    private val photosRepository: PhotosRepository
) : FlowUseCase<List<Photo>> {

    override fun call(): Flow<List<Photo>> =
        photosRepository.fetchPhotos()
}
