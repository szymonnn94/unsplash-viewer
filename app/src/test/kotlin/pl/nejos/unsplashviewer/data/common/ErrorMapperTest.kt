package pl.nejos.unsplashviewer.data.common

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Test
import pl.nejos.unsplashviewer.domain.exception.UnsplashViewerException
import java.io.IOException
import java.net.UnknownHostException

@ExperimentalCoroutinesApi
class ErrorMapperTest {
    private val errorMapper = ErrorMapper()

    @Test
    fun `Should throw ConnectionException when UnknownHostException thrown`() {
        val actual = errorMapper.map(UnknownHostException())
        assertEquals(UnsplashViewerException.ConnectionException, actual)
    }

    @Test
    fun `Should throw ConnectionException when IOException thrown`() {
        val actual = errorMapper.map(IOException())
        assertEquals(UnsplashViewerException.ConnectionException, actual)
    }

    @Test
    fun `Should throw UnknownException when RuntimeException thrown`() {
        val actual = errorMapper.map(RuntimeException())
        assertEquals(UnsplashViewerException.UnknownException, actual)
    }
}
