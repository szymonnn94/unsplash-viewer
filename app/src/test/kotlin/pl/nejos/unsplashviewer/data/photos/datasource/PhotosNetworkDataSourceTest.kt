package pl.nejos.unsplashviewer.data.photos.datasource

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import pl.nejos.unsplashviewer.BuildConfig
import pl.nejos.unsplashviewer.MockData.photoEntities
import pl.nejos.unsplashviewer.data.common.ErrorMapper
import pl.nejos.unsplashviewer.data.photos.api.PhotosService
import pl.nejos.unsplashviewer.data.photos.entity.PhotoEntity
import pl.nejos.unsplashviewer.data.photos.entity.UrlsEntity

@ExperimentalCoroutinesApi
class PhotosNetworkDataSourceTest {
    private val photosService = mockk<PhotosService> {
        coEvery { getPhotos(any()) } returns photoEntities
    }
    private val errorMapper = ErrorMapper()
    private val photosNetworkDataSource = PhotosNetworkDataSource(photosService, errorMapper)

    @Test
    fun `Should fetch photo entities`() = runBlockingTest {
        photosNetworkDataSource.fetchPhotoEntities().collect()

        coVerify {
            photosService.getPhotos(BuildConfig.CLIENT_ID)
        }
    }
}
