package pl.nejos.unsplashviewer.data.photos.mapper

import org.junit.Assert.assertEquals
import org.junit.Test
import pl.nejos.unsplashviewer.MockData.photo1
import pl.nejos.unsplashviewer.MockData.photoEntities
import pl.nejos.unsplashviewer.MockData.photoEntity1
import pl.nejos.unsplashviewer.MockData.photos
import pl.nejos.unsplashviewer.data.photos.entity.PhotoEntity
import pl.nejos.unsplashviewer.data.photos.entity.UrlsEntity
import pl.nejos.unsplashviewer.domain.entity.Photo

class PhotoMapperTest {
    private val photoMapper = PhotoMapper()

    @Test
    fun `Should create Photo from PhotoEntity`() {
        val result = photoMapper.from(photoEntity1)

        assertEquals(photo1, result)
    }

    @Test
    fun `Should create Photo list from PhotoEntity list`() {
        val result = photoMapper.fromList(photoEntities)

        assertEquals(photos, result)
    }
}
