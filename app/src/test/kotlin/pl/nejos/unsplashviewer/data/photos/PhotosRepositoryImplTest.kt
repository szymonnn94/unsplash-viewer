package pl.nejos.unsplashviewer.data.photos

import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import pl.nejos.unsplashviewer.MockData.photoEntities
import pl.nejos.unsplashviewer.MockData.photos
import pl.nejos.unsplashviewer.data.photos.datasource.PhotosNetworkDataSource
import pl.nejos.unsplashviewer.data.photos.entity.PhotoEntity
import pl.nejos.unsplashviewer.data.photos.entity.UrlsEntity
import pl.nejos.unsplashviewer.data.photos.mapper.PhotoMapper
import pl.nejos.unsplashviewer.domain.entity.Photo

@ExperimentalCoroutinesApi
class PhotosRepositoryImplTest {
    private val photosNetworkDataSource = mockk<PhotosNetworkDataSource> {
        coEvery { fetchPhotoEntities() } returns flow { emit(photoEntities) }
    }
    private val photoMapper = mockk<PhotoMapper> {
        every { fromList(any()) } returns photos
    }
    private val photosRepositoryImpl = PhotosRepositoryImpl(photosNetworkDataSource, photoMapper)

    @Test
    fun `Should fetch photos entities and map them to photos`() = runBlockingTest {
        photosRepositoryImpl.fetchPhotos().collect()

        coVerifyOrder {
            photosNetworkDataSource.fetchPhotoEntities()
            photoMapper.fromList(photoEntities)
        }
    }
}
