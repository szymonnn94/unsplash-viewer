package pl.nejos.unsplashviewer

import pl.nejos.unsplashviewer.data.photos.entity.PhotoEntity
import pl.nejos.unsplashviewer.data.photos.entity.UrlsEntity
import pl.nejos.unsplashviewer.domain.entity.Photo

object MockData {
    val photoEntity1 =
        PhotoEntity(id = "id-photo1", urls = UrlsEntity(regular = "url-to-photo1"))
    val photoEntity2 =
        PhotoEntity(id = "id-photo2", urls = UrlsEntity(regular = "url-to-photo2"))
    val photoEntities = listOf(photoEntity1, photoEntity2)

    val photo1 = Photo(id = "id-photo1", "url-to-photo1")
    val photo2 = Photo(id = "id-photo2", "url-to-photo2")
    val photos = listOf(photo1, photo2)
}
