package pl.nejos.unsplashviewer.presentation.photos

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.verifyOrder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import pl.nejos.unsplashviewer.MockData.photos
import pl.nejos.unsplashviewer.domain.exception.UnsplashViewerException
import pl.nejos.unsplashviewer.domain.usecase.GetPhotosUseCase

@ExperimentalCoroutinesApi
class PhotosViewModelTest {
    @get:Rule
    val liveDataRule = InstantTaskExecutorRule()

    private val getPhotosUseCase = mockk<GetPhotosUseCase>()
    private val viewModel = PhotosViewModel(getPhotosUseCase)
    private val stateObserver = mockk<Observer<PhotosState>>(relaxUnitFun = true)

    @Before
    fun setup() {
        Dispatchers.setMain(TestCoroutineDispatcher())
        viewModel.stateLiveData.observeForever(stateObserver)
    }

    @Test
    fun `Should show success when photos fetched successfully`() {
        coEvery { getPhotosUseCase.call() } returns flow { emit(photos) }

        viewModel.init()

        verifyOrder {
            stateObserver.onChanged(PhotosState.Loading)
            stateObserver.onChanged(PhotosState.Success(photos))
        }
    }

    @Test
    fun `Should show error when photos fetch failed`() {
        coEvery { getPhotosUseCase.call() } returns
                flow { throw UnsplashViewerException.UnknownException }

        viewModel.init()

        verifyOrder {
            stateObserver.onChanged(PhotosState.Loading)
            stateObserver.onChanged(
                PhotosState.Failure(UnsplashViewerException.UnknownException.displayMessage)
            )
        }
    }
}
