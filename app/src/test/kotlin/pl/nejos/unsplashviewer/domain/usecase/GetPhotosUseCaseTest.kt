package pl.nejos.unsplashviewer.domain.usecase

import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import pl.nejos.unsplashviewer.domain.repository.PhotosRepository

@ExperimentalCoroutinesApi
class GetPhotosUseCaseTest {
    private val photosRepository = mockk<PhotosRepository>(relaxed = true)
    private val getPhotosUseCase = GetPhotosUseCase(photosRepository)

    @Test
    fun `Should get photos from repository`() = runBlockingTest {
        getPhotosUseCase.call()

        coVerify {
            photosRepository.fetchPhotos()
        }
    }
}
